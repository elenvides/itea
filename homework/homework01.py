# Задание 1. Встроенные типы данных, операторы, функции и генераторы
#
# Напишите реализации объявленных ниже функций. Для проверки
# корректности реализации ваших функций, запустите тесты:
#
# pytest test_homework01.py
#
# Если написанный вами код не содержит синтаксических ошибок,
# вы увидите результаты тестов ваших решений.
import collections
from collections import Iterable
from functools import reduce
from operator import mul


def fac(n):
    """
    Факториал

    Факториал числа N - произведение всех целых чисел от 1 до N
    включительно. Например, факториал числа 5 - произведение
    чисел 1, 2, 3, 4, 5.

    Функция должна вернуть факториал аргумента, числа n.
    """
    # return math.factorial(n)

    # res = 1
    # if n > 1:
    #     for i in range(2, n+1):
    #         res *= i
    # return res

    # for x in range(1, n):
    #     n *= x
    # return n

    # return reduce(lambda x, y: x * y, range(1, n), n)
    return reduce(mul, range(1, n), n)


def gcd(a, b):
    """
    Наибольший общий делитель (НОД) для двух целых чисел.

    Предполагаем, что оба аргумента - положительные числа
    Один из самых простых способов вычесления НОД - метод Эвклида,
    согласно которому

    1. НОД(a, 0) = a
    2. НОД(a, b) = НОД(b, a mod b)

    (mod - операция взятия остатка от деления, в python - оператор '%')
    """
    while True:
        if b > a:
            a, b = b, a
        if (a % b) == 0:
            return b
        a = a % b


def fib():
    """
    Генератор для ряда Фибоначчи

    Вам необходимо сгенерировать бесконечный ряд чисел Фибоначчи,
    в котором каждый последующий элемент ряда является суммой двух
    предыдущих. Начало последовательности: 1, 1, 2, 3, 5, 8, 13, ..

    Подсказка по реализации: для бесконечного цикла используйте идиому

    while True:
      ..

    """

    a = b = 1
    while True:
        yield a
        a, b = b, a + b


def flatten(seq):
    """
    Функция, преобразующая вложенные последовательности любого уровня
    вложенности в плоские, одноуровневые.

    flatten([])
    []
    flatten([1, 2])
    [1, 2]
    flatten([1, [2, [3]]])
    [1, 2, 3]
    flatten([(1, 2), (3, 4)])
    [1, 2, 3, 4]
    """
    # res_list = []
    # for el in seq:
    #     if (not isinstance(el, collections.Sequence)) or isinstance(el, str):
    #         res_list.append(el)
    #     else:
    #         res_list += flatten(el)
    # return res_list

    for x in seq:
        if type(x) in {list, tuple}:
            yield from flatten(x)
        else:
            yield x


