from pprint import pprint
from itertools import chain

# Начнем с простейшей игры - крестики-нолики :)
# Реализуйте определение исхода игры на доске.
# Доска представлена кортежем.
# Крестик - 1, нолик - 0, пустые клетки обозначены None.
# Для визуализации я определил переменные с удобными именами.
# None я здесь именую _ (подчеркивание - валидное имя в Python).

X, O, _ = 1, 0, None

TEST_BOARD = (
    O, X, O,
    X, O, X,
    _, _, X
)

# Возможны четыре исхода, для них я тоже определил именованные константы.
O_WINS, X_WINS, UNDEFINED, DRAW = range(4)

# Первая подзадача и подсказка - реализуйте функцию,
# которая возвращает все возможные комбинации по 3 клетки в ряд:
# горизонтали, вертикали, диагонали. Таким образом эта часть
# задачи сводится к упражнению на слайсинг последовательности.
# Можете также попробовать реализовать как генератор.


def slice3(board):
    """
    Возвращает все возможные комбинации по 3 клетки в ряд:
    горизонтали, вертикали, диагонали.

    # >>> from pprint import pprint
    # >>> pprint(list(slice3((
    # ... O, X, O,
    # ... X, O, X,
    # ... _, _, X
    # ... ))))
    [(0, 1, 0),
     (1, 0, 1),
     (None, None, 1),
     (0, 1, None),
     (1, 0, None),
     (0, 1, 1),
     (0, 0, 1),
     (0, 0, None)]
    """
    board_list = list()

    # horizontal
    board_list.append(board[0:3])
    board_list.append(board[3:6])
    board_list.append(board[6:])
    # vertical
    board_list.append(board[::3])
    board_list.append(board[1::3])
    board_list.append(board[2::3])
    # diagonal
    board_list.append(board[::4])
    board_list.append(board[2:-2:2])

    # horiz = zip(*[iter(a)]*3)
    # vert = zip(horiz)
    # d1 = board[::4]
    # d2 = board[2:-2:2]
    # return chain(horiz, vert, d1, d2)

    return board_list


pprint(slice3(TEST_BOARD))


def outcome(board):
    """
    Определение исхода.
    O_WINS, X_WINS, UNDEFINED, DRAW = range(4)

    Допущение: доска может содержать только множество корректных состояний,
    которое могут получиться в результате справедливой игры.

    1. Когда на доске перечеркнуты первые 3 клетки - игра завершена.
    В этом случае вы должны определить, кто выиграл, и вернуть
    X_WINS или O_WINS.

    # >>> outcome((
    # ... X, X, O,
    # ... O, X, _,
    # ... O, _, X))
    1

    2. Для простоты мы пока предполагаем, что игра всегда доигрывается
    до конца. Пока есть пустые клетки и не перечеркнуты любые 3 -
    исход не определен - UNDEFINED.

    # >>> outcome((
    # ... X, X, O,
    # ... O, O, X,
    # ... _, _, X))
    2

    3. Если заполнены все клетки, и нет комбинации из трех смежных,
    выигрыш невозможен ни для кого, присуждаем ничью - DRAW

    # >>> outcome((
    # ... X, X, O,
    # ... O, O, X,
    # ... X, X, O))
    3
    """
    for i in slice3(board):
        if None not in i and len(set(i)) == 1:
            # return i[0]
            if i[0] == O:
                return O_WINS
            return X_WINS
    if None in board:
        return UNDEFINED
    return DRAW
    # уверенна, тут могло быть что-то вроде map(lambda i: <do smth with i>, slice3(board))


print(outcome(TEST_BOARD))


if __name__ == "__main__":
    import doctest
    doctest.testmod()
